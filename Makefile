.PHONY: all dep test vet clean build build-snap

all: dep vet test build

dep:
	@go get -v -d ./...

test: 
	@go test -race ./...

vet:
	@go vet ./...

clean:
	rm teagen_linux_*
	rm teagen_darwin_*
	rm teagen_windows_*


###################################################################
#  Build and rule generation for Linux, Mac, and Windows targets  #
###################################################################

# This generates the rules for go build. The arguments are:
# 1 - OS name
# 2 - Architecture name
# 3 - file suffix (including . )
#
# It also generates -snap rules. These rules builds without the -i argument 
# because it might cause error if Go was installed through Snap
define gen-build

$(1)-$(2):
	env GOOS=$(1) GOARCH=$(2) go build -i -o teagen_$(1)_$(2)$(3) ./cmd/teagen;

$(1)-$(2)-snap:
	env GOOS=$(1) GOARCH=$(2) go build -o teagen_$(1)_$(2)$(3) ./cmd/teagen;

endef

# Generate windows rules
windows-arch = 386 amd64
$(foreach arch, $(windows-arch), $(eval $(call gen-build,windows,$(arch),.exe)))
# Build windows targets
build: windows-386 windows-amd64
build-snap: windows-386-snap windows-amd64-snap

# Generate linux rules
linux-arch = 386 amd64 arm arm64 ppc64 ppc64le mips mipsle mips64 mips64le
$(foreach arch, $(linux-arch), $(eval $(call gen-build,linux,$(arch),)))
# Build linux targets
build: linux-386 linux-amd64 linux-arm linux-arm64 linux-ppc64 linux-ppc64le linux-mips linux-mipsle linux-mips64 linux-mips64le 
build-snap: linux-386-snap linux-amd64-snap linux-arm-snap linux-arm64-snap linux-ppc64-snap linux-ppc64le-snap linux-mips-snap linux-mipsle-snap linux-mips64-snap linux-mips64le-snap 

# Generate darwin rules
darwin-arch = 386 amd64
$(foreach arch, $(darwin-arch), $(eval $(call gen-build,darwin,$(arch),)))
# Build darwin targets
build: darwin-386 darwin-amd64
build-snap: darwin-386-snap darwin-amd64-snap
