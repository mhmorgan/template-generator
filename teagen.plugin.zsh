#compdef __teagen_complete teagen

# teagen zsh completion.
# Not finished.
# see http://zsh.sourceforge.net/Doc/Release/Completion-System.html

__teagen_complete() {

  # Set the flags of the program. The flags are given on the form:
  # name[description text]:argument:argument-options
  teagen_flags=(
    '-l[list all available template names]'
    '-h[print help message]'
    #'-n[choose template name.]:name:($(teagen -l))'
    '-o[choose output file.]:file'
    '1:file:($(teagen -l))'
  )
  
  # Set the approptriat arguments
  _arguments : ${teagen_flags[@]}
}

compdef __teagen_complete teagen
