{{- /* 
        C template. 
        Optional hello world.
*/ -}}

{{- $hello := ask "Add hello world" -}}

#include <stdlib.h>
{{- if $hello}}
#include <stdio.h>
{{- end}}

int
main(int argc, char** argv)
{ {{if $hello}}
        puts("Hello world");{{end}}
        return 0;
}

