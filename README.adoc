= Teagen
Magnus Hirth <mhmorgan42@gmail.com>
2018-11-27
:toc: macro
:!toc-title:

Teagen is a program which can generate text files from templates, for any kind
of file. The templates are text files themselves which may contain special
syntax for customizing the output by adding support for user input, optional
sections, and  date/time insertion.

Teagen was created to be used from a terminal and works with any text based file
format.

toc::[]

== Usage

Teagen creates a text file from either a default or a specified template. The
output file is named after the template file, unless otherwise specified. To 
specify a template, add its file name as an argument. To specify an output 
file use the -o option.

The default template location is `$HOME/templates/`

Terminal help text:

```
usage: teagen [-o file] template
       teagen -l | -h

Generate a file from a template.

options:
  -h	print this help message
  -l	list all known template files
  -o file
    	Choose output file
```


== Templates Syntax

Templates are text files which may contain special syntax for customizing
output. Any text inside `{{ .. }}` are interpreted as special syntax. For
details on the template syntax see the Go standard package 
https://golang.org/pkg/text/template/[text/template].

In addition to the functions described in
https://golang.org/pkg/text/template/[text/template], Teagen adds some
convenience functions more suited to dynamic creation of templates from the
terminal:

get :: Get input from the user. Takes one string as argument, which is the 
prompt text for the user.
ask :: Ask the user a yes/no question. Returns a bool value.
date :: Insert today's date, on format yyyy-mm-dd
time :: Insert current time, on format HH:MM:SS
year :: Insert current year.


== Example

Add basic template file to `$HOME/templates/`:

.hello.txt
	{{ get "Title: " }}
	{{if ask "Add date" -}}
	{{ date }}
	{{ end -}}
	Hello template world

Run Teagen:

	$ teagen -o output.txt hello.txt
	Title: Template Test
	Add date? (y/n)

Resulting output:

.output.txt
	Template Test
	2019-01-03
	Hello template world


