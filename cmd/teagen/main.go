package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"text/template"
	"time"

	"gitlab.com/mhmorgan/terminal/cli"
)

// magicArgs is a map which maps magic template functions, such as "date", to
// their respective functions which generates their string.
var functions = template.FuncMap{
	// Insert current date yyyy-mm-dd
	"date": func() string {
		return time.Now().Format("2006-01-02")
	},

	// Insert current year
	"year": func() string {
		return time.Now().Format("2006")
	},

	// Insert current time, HH:MM:SS
	"time": func() string {
		return time.Now().Format("15:04:05")
	},

	// The user can be asked to confirm a statement, returning a boolean
	"ask": cli.ConfirmString,

	// Arbitrary text can be received from the user using 'get'
	"get": cli.PromptString,
}

var (
	help      = flag.Bool("h", false, "print this help message")
	listFiles = flag.Bool("l", false, "list all known template files")
	outFile   = flag.String("o", "", "choose output `file`")
)

// Should allow multiple template directories, as specified in a config file.
// Should then separate between abs and rel path; rel path are relative to HOME
const tmplDir = "templates/"

func main() {
	log.SetFlags(0)

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
		os.Exit(2)
	}
	flag.Parse()

	// Lookup the HOME directory. Template directories with a relative path
	// must be relative to the HOME directory.
	home, ok := os.LookupEnv("HOME")
	if !ok {
		log.Fatal("did not find a home directory")
	}

	absTmplDir := filepath.Join(home, tmplDir)
	templates, err := ioutil.ReadDir(absTmplDir)
	if err != nil {
		log.Fatalf("Reading teagen directory: %v\n", err)
	}

	// Check for help or list flag. Any of these flags make the program exit
	switch {
	case *help:
		flag.Usage()

	case *listFiles:
		files := make([]string, 0)
		for _, t := range templates {
			files = append(files, t.Name())
		}
		sort.Strings(files)
		fmt.Println(strings.Join(files, "\n"))
		return
	}

	// Check for erroneous input
	if flag.NArg() > 1 {
		// Flags cannot appear after an anonymous argument because of
		// the way 'flag' does parsing.
		for _, arg := range flag.Args() {
			if strings.HasPrefix(arg, "-") {
				log.Fatalln("flags cannot appear after anonymous arguments")
			}
		}
		// Inform the user that only one template are used.
		log.Printf("got %d template files, only the first are used: %s\n", flag.NArg(), flag.Arg(0))
	} else if flag.NArg() == 0 {
		log.Fatalln("missing template file")
	}

	var (
		fin   *os.File
		fout  *os.File
		found bool

		tmplFile = flag.Arg(0)
	)

	// Verify that the given template file actually exists
	for _, t := range templates {
		if t.Name() == tmplFile {
			found = true
			break
		}
	}

	if !found {
		log.Fatalf("file not found: %v\n", tmplFile)
	}

	fin, err = os.Open(filepath.Join(absTmplDir, tmplFile))
	if err != nil {
		log.Fatalln(err)
	}
	defer fin.Close()

	b, err := ioutil.ReadAll(fin)
	if err != nil {
		log.Fatalf("reading %q: %v\n", fin.Name(), err)
	}

	// Parse the template before creating the output file, so an empty
	// output file isn't created in case of an erroneous template.
	tmpl, err := template.New(fin.Name()).Funcs(functions).Parse(string(b))
	if err != nil {
		log.Fatalf("parsing: %v\n", err)
	}

	// By default the output file is named the same as the template file
	if *outFile == "" {
		*outFile = tmplFile
	}

	// The output file cannot already exist in current directory
	fout, err = os.OpenFile(*outFile, os.O_WRONLY|os.O_CREATE|os.O_EXCL, 0666)
	if err != nil {
		log.Fatalln(err)
	}
	defer fout.Close()

	if err = tmpl.Execute(fout, nil); err != nil {
		log.Fatalf("executing: %v", err)
	}
}

const usage = `usage: teagen [-o file] template
       teagen -l | -h

Generate a file from a template.

options:
`
